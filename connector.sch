EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5850 3850 2    50   ~ 0
AI15
Text Label 5850 3550 2    50   ~ 0
AI14
Text Label 5850 3250 2    50   ~ 0
AI13
Text Label 5850 2950 2    50   ~ 0
AI12
Text Label 5850 2550 2    50   ~ 0
AI11
Text Label 5850 2250 2    50   ~ 0
AI10
Text Label 5850 1950 2    50   ~ 0
AI9
Text Label 5850 1650 2    50   ~ 0
AI8
Text Label 5850 3750 2    50   ~ 0
AI7
Text Label 5850 3450 2    50   ~ 0
AI6
Text Label 5850 3150 2    50   ~ 0
AI5
Text Label 5850 2850 2    50   ~ 0
AI4
Text Label 5850 2450 2    50   ~ 0
AI3
Text Label 5850 2150 2    50   ~ 0
AI2
Text Label 5850 1850 2    50   ~ 0
AI1
Text Label 5700 1550 0    50   ~ 0
AI0
NoConn ~ 5850 3850
NoConn ~ 5850 3550
NoConn ~ 5850 3250
NoConn ~ 5850 2950
NoConn ~ 5850 2550
NoConn ~ 5850 2250
NoConn ~ 5850 1950
NoConn ~ 5850 1650
Wire Wire Line
	5300 3650 5300 3950
Wire Wire Line
	5300 3350 5300 3650
Wire Wire Line
	5300 3050 5300 3350
Wire Wire Line
	5300 2650 5300 3050
Wire Wire Line
	5300 2350 5300 2650
Wire Wire Line
	5300 2050 5300 2350
Wire Wire Line
	5300 1750 5300 2050
$Comp
L power:GND #PWR0117
U 1 1 6075560C
P 5300 4250
F 0 "#PWR0117" H 5300 4000 50  0001 C CNN
F 1 "GND" H 5305 4077 50  0000 C CNN
F 2 "" H 5300 4250 50  0001 C CNN
F 3 "" H 5300 4250 50  0001 C CNN
	1    5300 4250
	1    0    0    -1  
$EndComp
Connection ~ 6150 4800
Wire Wire Line
	6150 4900 6150 4800
$Comp
L power:GND #PWR0118
U 1 1 60755604
P 6150 4900
F 0 "#PWR0118" H 6150 4650 50  0001 C CNN
F 1 "GND" H 6155 4727 50  0000 C CNN
F 2 "" H 6150 4900 50  0001 C CNN
F 3 "" H 6150 4900 50  0001 C CNN
	1    6150 4900
	1    0    0    -1  
$EndComp
Connection ~ 6950 4800
Wire Wire Line
	7350 4800 7350 4700
Wire Wire Line
	6950 4800 7350 4800
Connection ~ 6550 4800
Wire Wire Line
	6950 4800 6950 4700
Wire Wire Line
	6550 4800 6950 4800
Wire Wire Line
	6550 4800 6550 4700
Wire Wire Line
	6150 4800 6550 4800
Wire Wire Line
	6150 4700 6150 4800
Connection ~ 6950 4300
Wire Wire Line
	7350 4300 7350 4400
Wire Wire Line
	6950 4300 7350 4300
Connection ~ 6550 4300
Wire Wire Line
	6950 4300 6950 4400
Wire Wire Line
	6550 4300 6950 4300
Wire Wire Line
	6150 4300 6150 4400
Connection ~ 6150 4300
Wire Wire Line
	6550 4300 6150 4300
Wire Wire Line
	6550 4400 6550 4300
Wire Wire Line
	6150 4150 6150 4300
$Comp
L Device:C C58
U 1 1 607555EA
P 7350 4550
F 0 "C58" H 7465 4596 50  0000 L CNN
F 1 "10p" H 7465 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7388 4400 50  0001 C CNN
F 3 "~" H 7350 4550 50  0001 C CNN
	1    7350 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C57
U 1 1 607555E4
P 6950 4550
F 0 "C57" H 7065 4596 50  0000 L CNN
F 1 "10p" H 7065 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6988 4400 50  0001 C CNN
F 3 "~" H 6950 4550 50  0001 C CNN
	1    6950 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R20
U 1 1 607555DE
P 6550 4550
F 0 "R20" H 6620 4596 50  0000 L CNN
F 1 "100k" H 6620 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6480 4550 50  0001 C CNN
F 3 "~" H 6550 4550 50  0001 C CNN
	1    6550 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 607555D8
P 6150 4550
F 0 "R19" H 6220 4596 50  0000 L CNN
F 1 "100k" H 6220 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6080 4550 50  0001 C CNN
F 3 "~" H 6150 4550 50  0001 C CNN
	1    6150 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3950 5300 4250
Connection ~ 5300 3950
Wire Wire Line
	5850 3950 5300 3950
Connection ~ 5300 3650
Wire Wire Line
	5850 3650 5300 3650
Connection ~ 5300 3350
Wire Wire Line
	5850 3350 5300 3350
Connection ~ 5300 3050
Wire Wire Line
	5850 3050 5300 3050
Connection ~ 5300 2650
Wire Wire Line
	5850 2650 5300 2650
Connection ~ 5300 2350
Wire Wire Line
	5300 2350 5850 2350
Connection ~ 5300 2050
Wire Wire Line
	5300 2050 5850 2050
Wire Wire Line
	5850 1750 5300 1750
Text Label 5850 2750 2    50   ~ 0
AISENSE
$Comp
L Connector:DB25_Female_MountingHoles J18
U 1 1 607555C1
P 6150 2750
F 0 "J18" H 6330 2752 50  0000 L CNN
F 1 "DB25_Female_MountingHoles" H 6330 2661 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-25_Female_Vertical_P2.77x2.84mm_MountingHoles" H 6150 2750 50  0001 C CNN
F 3 " ~" H 6150 2750 50  0001 C CNN
	1    6150 2750
	1    0    0    -1  
$EndComp
Text HLabel 4900 1850 0    50   Input ~ 0
Channel1
Text HLabel 4900 2150 0    50   Input ~ 0
Channel2
Text HLabel 4900 2450 0    50   Input ~ 0
Channel3
Text HLabel 4900 2850 0    50   Input ~ 0
Channel4
Text HLabel 4900 3150 0    50   Input ~ 0
Channel5
Text HLabel 4900 3450 0    50   Input ~ 0
Channel6
Text HLabel 4900 3750 0    50   Input ~ 0
Channel7
Text HLabel 4900 1550 0    50   Input ~ 0
Channel0
Wire Wire Line
	4900 1550 5850 1550
Wire Wire Line
	4900 2850 5850 2850
Wire Wire Line
	5850 2450 4900 2450
Wire Wire Line
	4900 2150 5850 2150
Wire Wire Line
	5850 1850 4900 1850
Wire Wire Line
	4900 3150 5850 3150
Wire Wire Line
	4900 3750 5850 3750
Wire Wire Line
	5850 3450 4900 3450
Text HLabel 4350 6250 2    50   Output ~ 0
VREF
$Comp
L power:GND #PWR0116
U 1 1 60C1768C
P 4150 6250
F 0 "#PWR0116" H 4150 6000 50  0001 C CNN
F 1 "GND" V 4155 6122 50  0000 R CNN
F 2 "" H 4150 6250 50  0001 C CNN
F 3 "" H 4150 6250 50  0001 C CNN
	1    4150 6250
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 6250 4350 6250
$EndSCHEMATC
